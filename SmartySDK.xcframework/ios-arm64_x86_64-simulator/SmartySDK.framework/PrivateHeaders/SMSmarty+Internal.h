#import <SmartySDK/SMSmarty.h>

#import <SmartySDK/SMUnanimousVoting.h>
#import <SmartySDK/SMRetryer.h>

@class SMSmarty;
@class SMSmartfoxClient;
@class SMTaskManager;
@class SMRequestSender;
@class SMConnectionManager;
@class SMUserAccountManager;
@class SMUserManager;


extern const int kSMSmartyProtocolVersion;


@interface SMSmarty (Internal)

@property (nonatomic, strong, readonly) SMError *authenticationError;

@property (nonatomic, strong, readonly) NSString *zoneName;
@property (nonatomic, strong, readonly) SMSmartfoxClient *smartFoxClient;

@property (nonatomic, strong, readonly) SMTaskManager *taskManager;
@property (nonatomic, strong, readonly) SMRequestSender *requestSender;

@property (nonatomic, strong, readonly) SMUserAccountManager *userAccountManager;
@property (nonatomic, strong, readonly) SMUnanimousVoting *autologinVoting;

@property (nonatomic, strong, readonly) SMUserManager *userManager;
@property (nonatomic, strong, readonly) SMBuddyManager *buddyManager;
@property (nonatomic, strong, readonly) SMRoomManager *roomManager;
@property (nonatomic, strong, readonly) SMInvitationManager *invitationManager;

@property (nonatomic, strong, readonly) SMUser *mySelf;

- (instancetype)initWithServerAddress:(NSString *)serverAddress
                           serverPort:(NSInteger)serverPort
                             zoneName:(NSString *)zoneName
                  gameProtocolVersion:(NSInteger)gameProtocolVersion;

- (BOOL)isOnLine;

- (void)disconnect;

- (void)setAutoreconnectEnabled:(BOOL)enabled;

- (void)setAutologinEnabled:(BOOL)enabled;

- (BOOL)isAutoreconnectEnabled;

- (BOOL)isAutologinEnabled;

@end


@interface SMAutoreconnectAction : NSObject <SMRetryerAction>

@property (nonatomic, strong) NSString *serverAddress;
@property (nonatomic, assign) NSInteger port;

- (instancetype)initWithConnectionManager:(SMConnectionManager *)connectionManager;

@end


@interface SMAutologinAction : NSObject <SMRetryerAction>

@property (nonatomic, strong) NSString *zoneName;

- (instancetype)initWithUserAccountManager:(SMUserAccountManager *)userAccountManager;

@end


@interface SMKeepAliveAction : NSObject <SMRetryerAction>

- (instancetype)initWithSmartFox:(SMSmartfoxClient *)smartFox;

@end
