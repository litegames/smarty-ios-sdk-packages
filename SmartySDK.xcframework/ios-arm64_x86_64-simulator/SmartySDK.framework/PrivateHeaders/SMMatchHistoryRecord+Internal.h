#import <SmartySDK/SMMatchHistoryRecord.h>

#import <SmartySDK/SMSmarty.h>

#import <SFS2XAPIIOS/SmartFox2XClient.h>


@interface SMMatchHistoryRecord (Internal)

+ (instancetype)createFromSFSObject:(id<ISFSObject>)sfsObject smarty:(SMSmarty *)smarty;

@end
