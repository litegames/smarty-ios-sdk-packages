#import <SmartySDK/SMMatchPlayer.h>

#import <SmartySDK/SMSmarty.h>

#import <SFS2XAPIIOS/SmartFox2XClient.h>


@interface SMMatchPlayer (Internal)

+ (instancetype)createFromSFSObject:(id<ISFSObject>)sfsObject smarty:(SMSmarty *)smarty;

@end
