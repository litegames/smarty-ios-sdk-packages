#import <Foundation/Foundation.h>

@class SMUnanimousVoting;


@protocol SMVoter <NSObject>

@property (nonatomic, assign, readonly) BOOL vote;

@end


@interface SMBlockVoter : NSObject <SMVoter>

- (instancetype)initWithBlock:(BOOL (^)(void))block;

@end

/*
 * Impatient voter. Requests vote counting every time he changes his mind.
 */
@interface SMImpatientVoter : NSObject <SMVoter>

@property (nonatomic, assign) BOOL vote;

- (instancetype)initWithVote:(BOOL)vote voting:(SMUnanimousVoting *)voting;

@end


@protocol SMVotingDelegate <NSObject>

- (void)votingChanged:(SMUnanimousVoting *)voting;

@end


@interface SMUnanimousVoting : NSObject

@property (nonatomic, assign, readonly) BOOL result;
@property (nonatomic, weak) id<SMVotingDelegate> delegate;

- (void)requestVoteCounting;

- (void)addVoter:(id<SMVoter>)voter;
- (void)removeVoter:(id<SMVoter>)voter;
- (BOOL)containsVoter:(id<SMVoter>)voter;

@end
