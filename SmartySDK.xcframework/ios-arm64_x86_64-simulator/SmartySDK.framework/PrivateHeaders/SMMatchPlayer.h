#import <Foundation/Foundation.h>

#import <SmartySDK/SMUserInfo.h>


@interface SMMatchPlayer : NSObject

@property (nonatomic, strong, readonly) SMUserInfo *userInfo;
@property (nonatomic, assign, readonly) long long score;
@property (nonatomic, assign, readonly) BOOL winner;

@end
