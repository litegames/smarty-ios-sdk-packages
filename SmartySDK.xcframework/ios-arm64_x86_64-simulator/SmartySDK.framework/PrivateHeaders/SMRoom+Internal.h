#import <SmartySDK/SMRoom.h>

#import <SFS2XAPIIOS/ISFSObject.h>
#import <SFS2XAPIIOS/User.h>


@class SFSRoom;
@class BaseEvent;


extern NSString *const kSMRoomVariableCloseWhenOwnerLeave;


@interface SMRoom (Internal)

@property (strong, nonatomic, readonly) SMSmarty *smarty;
@property (strong, nonatomic, readonly) SFSRoom *sfsRoom;

- (instancetype)initWithSmarty:(SMSmarty *)smarty room:(SFSRoom *)room;

- (void)onSmartFoxUserEnterRoom:(id<User>)sfsUser;
- (void)onSmartFoxUserExitRoom:(id<User>)sfsUser;
- (void)onSmartFoxRoomVariablesUpdate:(BaseEvent *)event;
- (void)onSmartFoxExtensionRequest:(NSString *)cmd params:(id<ISFSObject>)params;

- (NSArray<NSNumber *> *)toSFSUsersIds:(NSArray<SMUser *> *)users;

@end