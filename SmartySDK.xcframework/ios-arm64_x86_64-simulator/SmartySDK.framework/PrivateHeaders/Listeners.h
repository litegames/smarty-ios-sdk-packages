#import <Foundation/Foundation.h>

@interface Listeners <ListenerType> : NSObject

- (id)initWithOwner:(id)owner;

- (void)addListener:(ListenerType)listener;
- (void)removeListener:(ListenerType)listener;
- (BOOL)containsListener:(ListenerType)listener;

- (void)notifyListenersWithBlock:(void (^)(ListenerType listener))block;
- (void)notifyListenersInReverseWithBlock:(void (^)(ListenerType listener))block;

@end
