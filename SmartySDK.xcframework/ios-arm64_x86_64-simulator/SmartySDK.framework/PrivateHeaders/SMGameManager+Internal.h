#import <SmartySDK/SMGameManager.h>

#import <SmartySDK/SMRetryer.h>
#import <SmartySDK/SMUnanimousVoting.h>

@class SMGameManager;
@class SMGameSettings;
@class SMLobbyRoom;
@class SMSmarty;


typedef NS_ENUM(NSInteger, SMGameManagerState) {
    kSMGameManagerStateNotConnected,
    kSMGameManagerStateNotAuthenticated,
    kSMGameManagerStateReady,
    kSMGameManagerStateJoiningLobby,
    kSMGameManagerStateLobby,
    kSMGameManagerStateAutomatching,
    kSMGameManagerStateWaitingForPlayersReady,
    kSMGameManagerStatePlaying,
    kSMGameManagerStateFinished
};

NSString *smGameManagerStateDescribe(NSInteger stateId);


@protocol SMGameManagerStateListener <NSObject>

- (void)gameManager:(SMGameManager *)gameManager didEnterState:(SMGameManagerState)state;
- (void)gameManager:(SMGameManager *)gameManager didExitState:(SMGameManagerState)state;

@end


@interface SMGameKeepAliveAction : NSObject <SMRetryerAction>

- (instancetype)initWithGameManager:(SMGameManager *)gameManager;

@end


@interface SMGameManager (Internal)

@property (strong, nonatomic, readonly) SMLobbyRoom *lobbyRoom;
@property (strong, nonatomic, readonly) NSString *lobbyRoomPassword;
@property (assign, nonatomic, readonly) SMGameManagerState state;

@property (nonatomic, strong, readonly) SMUnanimousVoting *keepAliveVoting;

- (instancetype)initWithSmarty:(SMSmarty *)smarty;

- (void)createAndJoinLobby:(SMGameSettings *)gameSettings;
- (void)joinLobby:(NSInteger)lobbyRoomId password:(NSString *)password;

- (void)startAutoMatching;

- (void)reset;

- (BOOL)containsStateListener:(id<SMGameManagerStateListener>)listener;
- (void)addStateListener:(id<SMGameManagerStateListener>)listener;
- (void)removeStateListener:(id<SMGameManagerStateListener>)listener;

@end
