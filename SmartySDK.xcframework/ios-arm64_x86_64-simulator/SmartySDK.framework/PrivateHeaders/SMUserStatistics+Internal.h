#import <SmartySDK/SMUserStatistics.h>

#import <SFS2XAPIIOS/SmartFox2XClient.h>


@interface SMUserStatistics (Internal)

+ (instancetype)createFromSFSObject:(id<ISFSObject>)sfsObject;

@end
