#import <SmartySDK/SMDataArray.h>

@protocol ISFSArray;


@interface SMDataArray (Internal)

@property (strong, nonatomic, readonly) id<ISFSArray> sfsArray;

+ (instancetype)createFromDataArray:(SMDataArray *)dataArray;

- (instancetype)initWithSfsArray:(id<ISFSArray>)sfsArray;

@end