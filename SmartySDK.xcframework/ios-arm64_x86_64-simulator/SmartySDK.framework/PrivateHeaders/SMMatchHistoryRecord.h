#import <Foundation/Foundation.h>

@class SMMatchPlayer;


@interface SMMatchHistoryRecord : NSObject

@property (nonatomic, strong, readonly) NSDate *date;
@property (nonatomic, assign, readonly) NSInteger duration;
@property (nonatomic, strong, readonly) NSArray<SMMatchPlayer *> *players;

@end
