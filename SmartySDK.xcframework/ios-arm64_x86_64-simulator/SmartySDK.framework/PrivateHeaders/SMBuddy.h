#import <Foundation/Foundation.h>

#import <SmartySDK/SMUserState.h>

@class SMBuddy;
@class SMUserProfile;


@protocol SMBuddyOnlineStateListener <NSObject>

- (void)buddyOnlineStateDidChange:(SMBuddy *)buddy;

@end

@protocol SMBuddyBlockStateListener <NSObject>

- (void)buddyBlockStateDidChange:(SMBuddy *)buddy;

@end

@protocol SMBuddyDataUpdateListener <NSObject>

- (void)buddyDataDidUpdate:(SMBuddy *)buddy;

@end


@interface SMBuddy : NSObject

@property (nonatomic, assign, readonly) NSInteger id;
@property (nonatomic, strong, readonly) NSString *name;
@property (nonatomic, strong, readonly) SMUserProfile *profile;

@property (nonatomic, assign, readonly) BOOL online;
@property (nonatomic, assign, readonly) SMUserState state;
@property (nonatomic, assign, readonly) BOOL blocked;

- (BOOL)isItMe;

- (BOOL)containsOnlineStateListener:(id<SMBuddyOnlineStateListener>)listener;
- (void)addOnlineStateListener:(id<SMBuddyOnlineStateListener>)listener;
- (void)removeOnlineStateListener:(id<SMBuddyOnlineStateListener>)listener;

- (BOOL)containsBlockStateListener:(id<SMBuddyBlockStateListener>)listener;
- (void)addBlockStateListener:(id<SMBuddyBlockStateListener>)listener;
- (void)removeBlockStateListener:(id<SMBuddyBlockStateListener>)listener;

- (BOOL)containsDataUpdateListener:(id<SMBuddyDataUpdateListener>)listener;
- (void)addDataUpdateListener:(id<SMBuddyDataUpdateListener>)listener;
- (void)removeDataUpdateListener:(id<SMBuddyDataUpdateListener>)listener;

@end
