#import <Foundation/Foundation.h>


@protocol SMRetryerActionCompletionHandler <NSObject>

- (void)retryerActionDidFinish;

@end


@protocol SMRetryerAction <NSObject>

- (void)runActionWithCompletionHandler:(id<SMRetryerActionCompletionHandler>)completionHandler;

@end


/*
 * Repeats defined action every 'delayMs' interval if enabled.
 * Waits until action finishes, before scheduling next retry.
 */
@interface SMRetryer : NSObject <SMRetryerActionCompletionHandler>

@property (nonatomic, assign, getter=isEnabled) BOOL enabled;

- (instancetype)initWithAction:(id<SMRetryerAction>)action name:(NSString *)name delayMs:(NSInteger)delay delayDeviationMs:(NSInteger)delayDeviationMs;

@end
