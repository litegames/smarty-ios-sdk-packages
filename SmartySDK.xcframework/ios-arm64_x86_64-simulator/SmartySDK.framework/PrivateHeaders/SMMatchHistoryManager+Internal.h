#import <SmartySDK/SMMatchHistoryManager.h>

#import <SmartySDK/SMSmarty+Internal.h>
#import <SmartySDK/SMMatchHistory+Internal.h>


@interface SMMatchHistoryManager (Internal)

- (instancetype)initWithSmarty:(SMSmarty *)smarty;

- (id<SMPendingResult>)getMatchHistoryAtPage:(NSInteger)page
                                    pageSize:(NSInteger)pageSize;

@end