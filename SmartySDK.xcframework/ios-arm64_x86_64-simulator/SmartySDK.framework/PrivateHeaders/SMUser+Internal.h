#import <SmartySDK/SMUser.h>

#import <SFS2XAPIIOS/User.h>
#import <SFS2XAPIIOS/ISFSArray.h>

@class SMSmarty;
@class SMUserVariable;


@interface SMUser (Internal)

@property (nonatomic, weak, readonly) id<User> user;

- (instancetype)initWithSmarty:(SMSmarty *)smarty sfsUser:(id<User>)sfsUser profile:(id<ISFSArray>)profile;

@end
