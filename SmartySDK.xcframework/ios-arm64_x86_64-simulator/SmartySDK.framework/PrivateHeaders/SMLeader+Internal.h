#import <SmartySDK/SMLeader.h>

#import <SFS2XAPIIOS/ISFSObject.h>

@class SMSmarty;


@interface SMLeader (Internal)

+ (instancetype)createFromSFSObject:(id<ISFSObject>)sfsObject smarty:(SMSmarty *)smarty;

@end