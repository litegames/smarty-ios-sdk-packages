#import <SmartySDK/SMUserManager.h>

#import <SmartySDK/SMPendingResult.h>

@class SMSmarty;


@interface SMUserManager (Internal)

- (instancetype)initWithSmarty:(SMSmarty *)smarty;

- (id<SMPendingResult>)findUserWithPattern:(NSString *)pattern page:(NSInteger)page pageSize:(NSInteger)pageSize;

@end
