#import <SmartySDK/SMData.h>

@protocol ISFSObject;


@interface SMData (Internal)

@property (nonatomic, strong, readonly) id<ISFSObject> sfsObject;

+ (instancetype)createFromData:(SMData *)data;

- (instancetype)initWithSFSObject:(id<ISFSObject>)object;

@end
