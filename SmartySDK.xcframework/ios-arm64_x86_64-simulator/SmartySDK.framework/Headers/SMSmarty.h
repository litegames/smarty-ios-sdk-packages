#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class SMSmarty;
@class SMErrorHandler;
@class SMInvitationManager;
@class SMLeaderboardManager;
@class SMMatchHistoryManager;
@class SMStatisticsManager;
@class SMError;
@class SMUser;
@class SMBuddyManager;
@class SMRoomManager;
@class SMGameManager;


NS_ASSUME_NONNULL_BEGIN

@protocol SMSmartyConnectionListener <NSObject>

- (void)smartyDidConnect:(SMSmarty *)smarty;
- (void)smartyDidFailedToConnect:(SMSmarty *)smarty;
- (void)smartyDidLostConnection:(SMSmarty *)smarty;

@end


@protocol SMSmartyAuthenticationListener <NSObject>

- (void)smartyDidLogIn:(SMSmarty *)smarty;
- (void)smartyDidFailToLogIn:(SMSmarty *)smarty error:(SMError *)error;
- (void)smartyDidLogOut:(SMSmarty *)smarty;

@end


@interface SMSmarty : NSObject

@property (nonatomic, assign, readonly) NSInteger gameProtocolVersion;

@property (nonatomic, assign, readonly) BOOL connected;
@property (nonatomic, assign, readonly) BOOL authenticated;

@property (nonatomic, strong, readonly) SMUser *mySelf;
@property (nonatomic, strong, readonly) SMErrorHandler *errorHandler;
@property (nonatomic, strong, readonly) SMLeaderboardManager *leaderboardManager;
@property (nonatomic, strong, readonly) SMMatchHistoryManager *matchHistoryManager;
@property (nonatomic, strong, readonly) SMStatisticsManager *statisticsManager;
@property (nonatomic, strong, readonly) SMGameManager *gameManager;

+ (NSString *)version;

- (BOOL)containsConnectionListener:(id<SMSmartyConnectionListener>)listener;
- (void)addConnectionListener:(id<SMSmartyConnectionListener>)listener;
- (void)removeConnectionListener:(id<SMSmartyConnectionListener>)listener;

- (BOOL)containsAuthenticationListener:(id<SMSmartyAuthenticationListener>)listener;
- (void)addAuthenticationListener:(id<SMSmartyAuthenticationListener>)listener;
- (void)removeAuthenticationListener:(id<SMSmartyAuthenticationListener>)listener;

- (void)didReceiveLocalNotification:(UILocalNotification *)notification;
- (void)handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(nonnull void (^)(void))completionHandler;

@end

NS_ASSUME_NONNULL_END
