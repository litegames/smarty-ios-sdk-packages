#import <Foundation/Foundation.h>

@protocol SMGGameState;


@protocol SMGGamePlayer;

@protocol SMGGamePlayerDelegate <NSObject>

- (void)gamePlayer:(id<SMGGamePlayer>)gamePlayer didChangeState:(id<SMGGameState>)gameState;

@end


@protocol SMGGamePlayer <NSObject>

@property (nonatomic, readonly, assign) NSInteger id;
@property (nonatomic, weak) id<SMGGamePlayerDelegate> delegate;

- (void)processGameState:(id<SMGGameState>)gameState;

@end
