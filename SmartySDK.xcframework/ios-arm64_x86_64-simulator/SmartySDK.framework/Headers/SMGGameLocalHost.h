#import <Foundation/Foundation.h>

#import <SmartySDK/SMGGameHost.h>

@protocol SMGGame;
@protocol SMGGamePlayer;


@interface SMGGameLocalHost : NSObject <SMGGameHost>

- (instancetype)initWithGame:(id<SMGGame>)game players:(NSArray<id<SMGGamePlayer>> *)players;

@end
