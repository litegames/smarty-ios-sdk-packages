#import <Foundation/Foundation.h>

#import <SmartySDK/SMPendingResult.h>


@interface SMPendingResultConverter : NSObject <SMPendingResult>

- (instancetype)initWithPendingResult:(id<SMPendingResult>)result
                            converter:(id (^)(id object))converter;

@end
