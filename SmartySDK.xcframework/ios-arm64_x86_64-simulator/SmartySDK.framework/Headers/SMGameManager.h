#import <Foundation/Foundation.h>

@class SMGameRoom;


@interface SMGameManager : NSObject

@property (strong, nonatomic, readonly) SMGameRoom *gameRoom;

- (void)cancel;

@end
