#import <Foundation/Foundation.h>

@protocol SMPendingResult;
@class SMError;


@interface SMPendingResults : NSObject

+ (void)pendingResult:(id<SMPendingResult>)pendingResult setOnFinishCallbackOrNotifyIfDone:(void (^)(id result, SMError *error))onFinishCallback;

@end
