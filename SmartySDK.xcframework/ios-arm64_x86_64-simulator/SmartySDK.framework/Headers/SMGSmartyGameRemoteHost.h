#import <Foundation/Foundation.h>
#import <SmartySDK/SmartySDK.h>

#import <SmartySDK/SMGGameHost.h>

@protocol SMGGamePlayer;
@protocol SMGGameState;
@protocol SMGGameStateSerializer;


@interface SMGSmartyGameRemoteHost : NSObject <SMGGameHost>

@property (nonatomic, assign) BOOL sendGameStateDiff;

- (instancetype)initWithSmarty:(SMSmarty *)smarty
                      gameRoom:(SMGameRoom *)gameRoom
                    gamePlayer:(id<SMGGamePlayer>)gamePlayer
                     gameState:(id<SMGGameState>)gameState
           gameStateSerializer:(id<SMGGameStateSerializer>)gameStateSerializer;

@end
