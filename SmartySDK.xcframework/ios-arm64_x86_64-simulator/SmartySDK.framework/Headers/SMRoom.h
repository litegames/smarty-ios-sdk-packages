#import <Foundation/Foundation.h>


@class SMSmarty;
@class SMUser;
@class SMRoom;


@protocol SMRoomUserListener <NSObject>

- (void)room:(SMRoom *)room userDidEnter:(SMUser *)user;
- (void)room:(SMRoom *)room userDidExit:(SMUser *)user;

@end


@interface SMRoom : NSObject

@property (nonatomic, assign, readonly) NSInteger id;
@property (nonatomic, strong, readonly) NSString *name;
@property (nonatomic, assign, readonly) NSInteger maxUsers;
@property (nonatomic, strong, readonly) NSArray<SMUser *> *joinedUsers;

- (SMUser *)getUserById:(NSInteger)userId;

- (BOOL)containsUserListener:(id<SMRoomUserListener>)listener;
- (void)addUserListener:(id<SMRoomUserListener>)listener;
- (void)removeUserListener:(id<SMRoomUserListener>)listener;

@end
