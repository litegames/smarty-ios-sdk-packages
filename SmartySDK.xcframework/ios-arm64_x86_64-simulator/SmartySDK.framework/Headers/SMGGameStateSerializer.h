#import <Foundation/Foundation.h>
#import <SmartySDK/SmartySDK.h>

@protocol SMGGameState;


@protocol SMGGameStateSerializer <NSObject>

- (id<SMGGameState>)gameStateFromData:(SMData *)data;

- (SMData *)gameStateToData:(id<SMGGameState>)gameState;

@end
