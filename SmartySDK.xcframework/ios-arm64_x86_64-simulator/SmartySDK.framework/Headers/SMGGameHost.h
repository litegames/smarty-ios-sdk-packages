#import <Foundation/Foundation.h>


@protocol SMGGameHost <NSObject>

- (void)start;

@end
