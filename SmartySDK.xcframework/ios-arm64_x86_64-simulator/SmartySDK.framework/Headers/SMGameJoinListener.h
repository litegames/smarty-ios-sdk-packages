#import <Foundation/Foundation.h>


@protocol SMGameJoinListener <NSObject>

- (void)onGameJoin:(id)sender lobbyRoomId:(NSInteger)lobbyRoomId lobbyRoomPassword:(NSString *)lobbyRoomPassword;

@end
