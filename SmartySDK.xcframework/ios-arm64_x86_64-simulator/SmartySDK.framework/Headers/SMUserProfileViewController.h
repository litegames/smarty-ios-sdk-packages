#import <UIKit/UIKit.h>

#import <SmartySDK/SMSmarty.h>


@class SMUserProfileViewController;


@protocol SMUserProfileViewControllerDelegate <NSObject>

- (void)userProfileViewControllerDidDismiss:(SMUserProfileViewController *)userProfileViewController;

@end


@interface SMUserProfileViewController : UIViewController

@property (nonatomic, weak) id<SMUserProfileViewControllerDelegate> delegate;

@end
