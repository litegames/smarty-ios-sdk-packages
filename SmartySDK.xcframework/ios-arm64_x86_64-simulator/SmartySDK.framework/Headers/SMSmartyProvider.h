#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class SMSmarty;

@protocol SMGameCreateListener;
@protocol SMGameJoinListener;

@interface SMSmartyProvider : NSObject

+ (void)initializeWithServerAddress:(NSString *)serverAddress
                         serverPort:(NSInteger)serverPort
                           zoneName:(NSString *)zoneName
                gameProtocolVersion:(NSInteger)gameProtocolVersion;
+ (instancetype)sharedInstance;

@property (nonatomic, strong, readonly) SMSmarty *smarty;

- (BOOL)containsGameCreateListener:(id<SMGameCreateListener>)listener;
- (void)addGameCreateListener:(id<SMGameCreateListener>)listener;
- (void)removeGameCreateListener:(id<SMGameCreateListener>)listener;

- (BOOL)containsGameJoinListener:(id<SMGameJoinListener>)listener;
- (void)addGameJoinListener:(id<SMGameJoinListener>)listener;
- (void)removeGameJoinListener:(id<SMGameJoinListener>)listener;

@end
