#import <Foundation/Foundation.h>

@protocol SMGGameState <NSObject, NSCopying>

- (BOOL)finished;

- (BOOL)hasPlayerQuit:(NSInteger)playerId;
- (void)playerQuit:(NSInteger)playerId;

- (BOOL)isPlayerQuitter:(NSInteger)playerId;
- (BOOL)isPlayerWinner:(NSInteger)playerId;

- (long long)playerScore:(NSInteger)playerId;

- (NSString *)leaderboardIdentifier;

- (id<SMGGameState>)copy;

@end
