#import <Foundation/Foundation.h>

#import <SmartySDK/SMRoom.h>


@class SMUser;
@class SMData;
@class SMGameRoom;


typedef NS_ENUM(NSInteger, SMGameRoomState) {
    kSMGameRoomStateWaitingForPlayersReady,
    kSMGameRoomStatePlaying,
    kSMGameRoomStateFinished
};

NSString *smGameRoomStateDescribe(SMGameRoomState state);


@protocol SMGameRoomStateListener <NSObject>

- (void)gameRoomDidChangeState:(SMGameRoom *)gameRoom;

@end


@protocol SMGameRoomUserResourcesLoadedListener <NSObject>

- (void)gameRoom:(SMGameRoom *)gameRoom userDidLoadResources:(SMUser *)user;

@end


@protocol SMGameRoomMessageListener <NSObject>

- (void)gameRoom:(SMGameRoom *)gameRoom didReceiveMessage:(SMData *)message fromUser:(SMUser *)user;

@end


@interface SMGameRoom : SMRoom

@property (nonatomic, assign, readonly) SMGameRoomState state;
@property (nonatomic, assign, readonly) BOOL meStarts;
@property (nonatomic, strong, readonly) SMUser *startingPlayer;

- (BOOL)userResourcesLoadedForUser:(SMUser *)user;
- (void)setMyResourcesLoaded;

- (void)sendMessage:(SMData *)message;
- (void)sendMessage:(SMData *)message recipient:(SMUser *)recipient;
- (void)sendMessage:(SMData *)message recipients:(NSArray<SMUser *> *)recipients;

- (BOOL)containsStateListener:(id<SMGameRoomStateListener>)listener;
- (void)addStateListener:(id<SMGameRoomStateListener>)listener;
- (void)removeStateListener:(id<SMGameRoomStateListener>)listener;

- (BOOL)containsUserResourcesLoadedListener:(id<SMGameRoomUserResourcesLoadedListener>)listener;
- (void)addUserResourcesLoadedListener:(id<SMGameRoomUserResourcesLoadedListener>)listener;
- (void)removeUserResourcesLoadedListener:(id<SMGameRoomUserResourcesLoadedListener>)listener;

- (BOOL)containsMessageListener:(id<SMGameRoomMessageListener>)listener;
- (void)addMessageListener:(id<SMGameRoomMessageListener>)listener;
- (void)removeMessageListener:(id<SMGameRoomMessageListener>)listener;

@end
