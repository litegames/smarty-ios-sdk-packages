#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "SmartySDK.h"
#import "SMSmartyProvider.h"
#import "SMSmarty.h"
#import "SMData.h"
#import "SMDataArray.h"
#import "SMDataWrapper.h"
#import "SMError.h"
#import "SMErrorPrinter.h"
#import "SMUser.h"
#import "SMUserProfile.h"
#import "SMUserVariable.h"
#import "SMResource.h"
#import "SMResourceSize.h"
#import "SMResourcesProvider.h"
#import "SMGameRoom.h"
#import "SMGameManager.h"
#import "SMGameSettings.h"
#import "SMRoom.h"
#import "SMLeaderboardManager.h"
#import "SMMatchHistoryManager.h"
#import "SMStatisticsManager.h"
#import "SMUserStatistics.h"
#import "SMGameCreateListener.h"
#import "SMGameJoinListener.h"
#import "SMLeaderboardViewController.h"
#import "SMMatchHistoryViewController.h"
#import "SMMatchMakingViewController.h"
#import "SMPlayersViewController.h"
#import "SMUserProfileViewController.h"
#import "SMPendingResult.h"
#import "SMPendingResultConverter.h"
#import "SMPendingResults.h"
#import "SMUserAvatarView.h"
#import "SMUserDisplayNameView.h"
#import "SMErrorCodes.h"
#import "SMErrorHandler.h"
#import "SMGGame.h"
#import "SMGGameHost.h"
#import "SMGGameLocalHost.h"
#import "SMGGamePlayer.h"
#import "SMGGameState.h"
#import "SMGGameStateSerializer.h"
#import "SMGSmartyGameRemoteHost.h"
#import "SMGSmartyLocalPlayer.h"
#import "SMGSmartyRemotePlayer.h"

FOUNDATION_EXPORT double SmartySDKVersionNumber;
FOUNDATION_EXPORT const unsigned char SmartySDKVersionString[];

