#import <Foundation/Foundation.h>

@class SMMatchHistoryRecord;


@interface SMMatchHistory : NSObject

@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) NSInteger pageSize;
@property (nonatomic, strong) NSArray<SMMatchHistoryRecord *> *matches;

@end
