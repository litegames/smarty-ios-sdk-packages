#import <Foundation/Foundation.h>

@class SMUserInfo;


@interface SMLeader : NSObject

@property (nonatomic, strong, readonly) SMUserInfo *userInfo;
@property (nonatomic, assign, readonly) long long score;
@property (nonatomic, assign, readonly) NSInteger rank;
@property (nonatomic, assign, readonly) NSInteger rankPercentage;

@end
