#import <SmartySDK/SMBuddyManager.h>

@class SMSmarty;

@interface SMBuddyManager (Internal)

- (instancetype)initWithSmarty:(SMSmarty *)smarty;

@end