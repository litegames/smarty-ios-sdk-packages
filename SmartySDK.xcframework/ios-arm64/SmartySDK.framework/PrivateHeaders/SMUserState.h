#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, SMUserState) {
    kSMUserStateAvailable,
    kSMUserStateInvisible,
    kSMUserStateUnknown = -1,
};

NSString *smUserStateName(SMUserState userState);
SMUserState smUserStateFromSfsState(NSString *sfsState);
