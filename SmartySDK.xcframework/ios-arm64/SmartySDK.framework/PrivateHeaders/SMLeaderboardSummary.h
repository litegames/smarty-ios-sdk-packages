#import <Foundation/Foundation.h>

@class SMLeader;


extern const NSInteger kSMLeaderbaordGoalHighestScoreSum;
extern const NSInteger kSMLeaderbaordGoalHighestScore;
extern const NSInteger kSMLeaderbaordGoalLowestScore;


@interface SMLeaderboardSummary : NSObject

@property (nonatomic, strong, readonly) NSString *identifier;
@property (nonatomic, strong, readonly) NSString *title;
@property (nonatomic, assign, readonly) NSInteger goal;
@property (nonatomic, strong, readonly) NSArray<SMLeader *> *leaders;
@property (nonatomic, assign, readonly) NSNumber *gapIndex;
@property (nonatomic, assign, readonly) NSNumber *myselfIndex;

@end
