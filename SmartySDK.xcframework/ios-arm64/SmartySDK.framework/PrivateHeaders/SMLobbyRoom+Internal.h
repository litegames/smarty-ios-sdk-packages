#import <SmartySDK/SMLobbyRoom.h>

@class SFSRoom;


extern NSString *const kSMLobbyRoomVariableLobby;


@interface SMLobbyRoom (Internal)

+ (RoomSettings *)createRoomSettingsUsingName:(NSString *)name
                                     password:(NSString *)password
                                 gameSettings:(SMGameSettings *)gameSettings
                                      ownerId:(NSInteger)ownerId;

- (instancetype)initWithSmarty:(SMSmarty *)smarty room:(SFSRoom *)room;

- (void)play;
- (void)cancel;
- (void)restart;

@end