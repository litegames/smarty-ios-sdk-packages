#import <SmartySDK/SMUserVariable.h>

#import <SFS2XAPIIOS/UserVariable.h>


@interface SMUserVariable (Internal)

@property (nonatomic, strong, readonly) id<UserVariable> sfsUserVariable;

- (instancetype)initWithSFSUserVariable:(id<UserVariable>)sfsUserVariable;

@end
