#import <SmartySDK/SMGameRoom.h>


@class SFSRoom;


@protocol SMGameRoomCountdownListener <NSObject>

- (void)gameRoomCountdown:(SMGameRoom *)gameRoom;

@end


@interface SMGameRoom (Internal)

@property (nonatomic, assign, readonly) NSNumber *countdownValue;

- (instancetype)initWithSmarty:(SMSmarty *)smarty room:(SFSRoom *)room;

- (void)sendReady;

- (void)sendPong;

- (BOOL)containsCountdownListener:(id<SMGameRoomCountdownListener>)listener;
- (void)addCountdownListener:(id<SMGameRoomCountdownListener>)listener;
- (void)removeCountdownListener:(id<SMGameRoomCountdownListener>)listener;

@end
