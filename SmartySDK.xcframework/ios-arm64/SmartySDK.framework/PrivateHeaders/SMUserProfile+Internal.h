#import <SmartySDK/SMUserProfile.h>


#import <SmartySDK/SMError.h>
#import <SmartySDK/SMSmarty+Internal.h>
#import <SmartySDK/SMData+Internal.h>
#import <SmartySDK/SMPendingResult.h>

@class SMDataArray;
@protocol ISFSArray;


extern NSString *const kSMUserProfileVariableNameDisplayName;
extern NSString *const kSMUserProfileVariableNameAvatar;
extern NSString *const kSMUserProfileVariableNameGender;
extern NSString *const kSMUserProfileVariableNameYearOfBirth;
extern NSString *const kSMUserProfileVariableNameCity;
extern NSString *const kSMUserProfileVariableNameCountry;
extern NSString *const kSMUserProfileVariableNameEmail;
extern NSString *const kSMUserProfileVariableNameFacebookId;


@interface SMUserProfile (Internal)

@property (nonatomic, strong, readonly) NSString *facebookId;
@property (nonatomic, weak, readonly) SMUser *owner;

+ (NSArray<NSString *> *)avatarsForPlayers;
+ (NSString *)avatarForAIPlayer;
+ (NSString *)avatarForOfflineUsage;

- (instancetype)initWithOwner:(SMUser *)owner profile:(id<ISFSArray>)profile listenForUpdates:(BOOL)listenForUpdates smarty:(SMSmarty *)smarty;

- (void)onProfileUpdate:(id<ISFSArray>)profile;

- (NSInteger)size;

- (BOOL)containsVariableNamed:(NSString *)key;

- (NSNumber *)boolVariableNamed:(NSString *)name;
- (id<SMPendingResult>)setBoolVariable:(NSNumber *)value named:(NSString *)name;
- (NSNumber *)intVariableNamed:(NSString *)name;
- (id<SMPendingResult>)setIntVariable:(NSNumber *)value named:(NSString *)name;
- (NSString *)stringVariableNamed:(NSString *)name;
- (id<SMPendingResult>)setStringVariable:(NSString *)value named:(NSString *)name;

- (SMDataArray *)toDataArray;

@end
