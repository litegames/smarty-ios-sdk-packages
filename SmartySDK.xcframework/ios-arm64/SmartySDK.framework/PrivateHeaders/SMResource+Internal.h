#import <SmartySDK/SMResource.h>


@interface SMResource (Internal)

+ (instancetype)localResourceWithSize:(SMResourceSize)size
                            imageName:(NSString *)imageName
                               bundle:(NSBundle *)bundle;

+ (instancetype)remoteResourceWithSize:(SMResourceSize)size
                                   url:(NSString *)url
                    fallbackImageNamed:(NSString *)fallbackImageName
                                bundle:(NSBundle *)bundle;

@end
