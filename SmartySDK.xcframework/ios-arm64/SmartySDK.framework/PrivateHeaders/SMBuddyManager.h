#import <Foundation/Foundation.h>

@class SMBuddyManager;
@class SMBuddy;

typedef NS_ENUM(NSInteger, SMBuddyManagerState) {
    kSMBuddyManagerStateNotConnected,
    kSMBuddyManagerStateNotAuthenticated,
    kSMBuddyManagerStateInitializing,
    kSMBuddyManagerStateReady
};

typedef NS_ENUM(NSInteger, SMBuddyManagerEventType) {
    kSMBuddyManagerEventConnection,
    kSMBuddyManagerEventConnectionLost,
    kSMBuddyManagerEventLogin,
    kSMBuddyManagerEventLogout,

    kSMBuddyManagerEventBuddyListInit,
    kSMBuddyManagerEventBuddyListError,
    kSMBuddyManagerEventAddBuddy,
    kSMBuddyManagerEventAddBlockedBuddy,
    kSMBuddyManagerEventRemoveBuddy
};


@protocol SMBuddyManagerStateListener <NSObject>

- (void)buddyManager:(SMBuddyManager *)buddyManager didEnterState:(SMBuddyManagerState)state;
- (void)buddyManager:(SMBuddyManager *)buddyManager didExitState:(SMBuddyManagerState)state;

@end


@protocol SMBuddyManagerBuddyListener <NSObject>

- (void)buddyManager:(SMBuddyManager *)buddyManager didAddBuddy:(SMBuddy *)buddy;
- (void)buddyManager:(SMBuddyManager *)buddyManager didRemoveBuddy:(SMBuddy *)buddy;
- (void)buddyManager:(SMBuddyManager *)buddyManager didBlockBuddy:(SMBuddy *)buddy;

@end


@interface SMBuddyManager : NSObject

@property (nonatomic, assign, readonly) SMBuddyManagerState state;
@property (nonatomic, strong, readonly) SMBuddy *mySelf;
@property (nonatomic, strong, readonly) NSArray<SMBuddy *> *myBuddies;

- (SMBuddy *)getBuddyById:(NSInteger)buddyId;
- (SMBuddy *)getBuddyByName:(NSString *)name;

- (void)addBuddy:(NSString *)name;
- (void)addBlockedBuddy:(NSString *)name;
- (void)removeBuddy:(NSString *)name;

- (BOOL)containsStateListener:(id<SMBuddyManagerStateListener>)listener;
- (void)addStateListener:(id<SMBuddyManagerStateListener>)listener;
- (void)removeStateListener:(id<SMBuddyManagerStateListener>)listener;

- (BOOL)containsBuddyListener:(id<SMBuddyManagerBuddyListener>)listener;
- (void)addBuddyListener:(id<SMBuddyManagerBuddyListener>)listener;
- (void)removeBuddyListener:(id<SMBuddyManagerBuddyListener>)listener;

@end
