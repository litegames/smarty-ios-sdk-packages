#import <SmartySDK/SMStatisticsManager.h>

#import <SmartySDK/SMSmarty+Internal.h>


@interface SMStatisticsManager (Internal)

- (instancetype)initWithSmarty:(SMSmarty *)smarty;

@end
