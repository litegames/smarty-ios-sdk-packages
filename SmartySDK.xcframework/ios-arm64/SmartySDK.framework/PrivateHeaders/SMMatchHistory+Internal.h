#import <SmartySDK/SMMatchHistory.h>

#import <SmartySDK/SMSmarty.h>

#import <SFS2XAPIIOS/SmartFox2XClient.h>


@interface SMMatchHistory (Internal)

+ (instancetype)createFromSFSObject:(id<ISFSObject>)sfsObject smarty:(SMSmarty *)smarty;

@end
