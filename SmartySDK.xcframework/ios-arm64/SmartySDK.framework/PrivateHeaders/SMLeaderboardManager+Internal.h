#import <SmartySDK/SMLeaderboardManager.h>

#import <SmartySDK/SMSmarty+Internal.h>
#import <SmartySDK/SMLeaderboardSummary+Internal.h>

typedef NS_ENUM(NSInteger, SMLeaderboardSummaryType) {
    kSMLeaderboardSummaryTypeOverall = 0,
    kSMLeaderboardSummaryTypeMonthly = 1,
    kSMLeaderboardSummaryTypeWeekly = 2,
};


@interface SMLeaderboardManager (Internal)

- (instancetype)initWithSmarty:(SMSmarty *)smarty;

- (id<SMPendingResult>)getLeaderboardSummaryForIdentifier:(NSString *)leaderboardIdentifier
                                               podiumSize:(NSInteger)podiumSize
                                          surroundingSize:(NSInteger)surroundingSize
                                              summaryType:(SMLeaderboardSummaryType)summaryType
                                                 language:(NSString *)language;

@end
