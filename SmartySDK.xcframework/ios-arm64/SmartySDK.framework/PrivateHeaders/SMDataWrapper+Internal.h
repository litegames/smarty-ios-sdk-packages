#import <SmartySDK/SMDataWrapper.h>

@class SFSDataWrapper;


@interface SMDataWrapper (Internal)

@property (nonatomic, strong, readonly) SFSDataWrapper *sfsDataWrapper;

- (instancetype)initWithSfsDataWrapper:(SFSDataWrapper *)sfsDataWrapper;

@end