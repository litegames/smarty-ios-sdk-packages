#import <SmartySDK/SMResourcesProvider.h>

#import <SmartySDK/SMUserState.h>


@interface SMResourcesProvider (Internal)

- (SMResource *)localAvatarResource:(NSString *)avatar size:(SMResourceSize)size;

- (NSArray<NSString *> *)countriesCodes;

- (UIImage *)buddyStateImage:(BOOL)online state:(SMUserState)state size:(SMResourceSize)size;
- (NSString *)buddyStateName:(BOOL)online state:(SMUserState)state;
- (UIImage *)buddyStateAvatarMaskImageWithSize:(SMResourceSize)size;
- (UIImage *)buddyStateAvatarAlphaMaskImageWithSize:(SMResourceSize)size;

@end
