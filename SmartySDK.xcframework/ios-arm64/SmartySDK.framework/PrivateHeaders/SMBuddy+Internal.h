#import <SmartySDK/SMBuddy.h>

#import <SFS2XAPIIOS/Buddy.h>

@class SMSmarty;
@class SMUserProfile;
@class SMBuddy;


@interface SMBuddy (Internal)

@property (nonatomic, assign) BOOL online;
@property (nonatomic, assign) SMUserState state;
@property (nonatomic, assign) BOOL blocked;

- (instancetype)initWithBuddy:(id<Buddy>)sfsBuddy smarty:(SMSmarty *)smarty;

- (void)onSmartFoxBuddyOnlineStateChange;
- (void)onSmartFoxBuddyBlock;
- (void)onSmartFoxBuddyVariablesUpdate:(NSArray<NSString *> *)changedVars;

@end
