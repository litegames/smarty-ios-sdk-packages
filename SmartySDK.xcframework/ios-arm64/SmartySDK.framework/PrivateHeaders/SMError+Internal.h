#import <SmartySDK/SMError.h>

#import <SFS2XAPIIOS/SmartFox2XClient.h>
#import <SmartySDK/SMSmartFoxErrorCodes.h>


extern NSString *const kSMErrorCodeSmartFoxError;
extern NSString *const kSMErrorCodeConnectionError;
extern NSString *const kSMErrorCodeAuthenticationError;
extern NSString *const kSMErrorCodeAuthenticationApiVersionObsoleteError;
extern NSString *const kSMErrorCodeAuthenticationUserNameNotRecognizedError;
extern NSString *const kSMErrorCodeAuthenticationWrongPasswordForUserError;

extern NSString *const kSMErrorCodeDisconnected;

extern NSString *const kSMErrorParamMsg;
extern NSString *const kSMErrorParamErrorCode;


@interface SMError (Internal)

@property (nonatomic, assign, readonly) BOOL valid;
@property (nonatomic, strong, readonly) id<ISFSObject> params;

@property (nonatomic, strong, readonly) NSString *fieldName;

+ (instancetype)internalError;
+ (instancetype)disconnectedError;
+ (instancetype)connectionError;

+ (instancetype)createAuthenticationError:(short)errorCode;

+ (instancetype)valueTooShortErrorWithMinLength:(NSInteger)minLength forField:(NSString *)fieldName;
+ (instancetype)smartFoxErrorWithCode:(short)errorCode message:(NSString *)msg;

- (instancetype)initWithError:(id<ISFSObject>)error;
- (instancetype)initWithError:(NSString *)code andParams:(id<ISFSObject>)params forField:(NSString *)fieldName;

- (short)getShortParam:(NSString *)name;

- (NSString *)getDump:(BOOL)format;

@end
