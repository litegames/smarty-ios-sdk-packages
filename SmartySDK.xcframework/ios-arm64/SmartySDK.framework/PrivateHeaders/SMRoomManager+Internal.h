#import <SmartySDK/SMRoomManager.h>

@class SMError;
@class SMRoomManager;
@class SMRoom;
@class SMSmarty;
@class SMGameSettings;


@protocol SMRoomManagerRoomListener <NSObject>

- (void)roomManager:(SMRoomManager *)roomManager onRoomJoin:(SMRoom *)room;
- (void)roomManager:(SMRoomManager *)roomManager onRoomJoinError:(SMError *)error;
- (void)roomManager:(SMRoomManager *)roomManager onRoomLeave:(SMRoom *)room;

@end


@interface SMRoomManager ()

- (instancetype)initWithSmarty:(SMSmarty *)smarty;

- (void)createAndJoinLobbyRoom:(NSString *)roomName password:(NSString *)password gameSettings:(SMGameSettings *)gameSettings;
- (void)joinLobbyRoom:(NSInteger)lobbyRoomId password:(NSString *)password;
- (void)leaveRoom:(SMRoom *)room;

- (BOOL)containsRoomListener:(id<SMRoomManagerRoomListener>)listener;
- (void)addRoomListener:(id<SMRoomManagerRoomListener>)listener;
- (void)removeRoomListener:(id<SMRoomManagerRoomListener>)listener;

@end
