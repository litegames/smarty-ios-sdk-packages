#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SMResourceSize) {
    kSMResourceSizeSmall,
    kSMResourceSizeNormal,
    kSMResourceSizeLarge,
};

CGSize smResourceSizeInPt(SMResourceSize resourceSize);
CGSize smResourceSizeInPx(SMResourceSize resourceSize);
