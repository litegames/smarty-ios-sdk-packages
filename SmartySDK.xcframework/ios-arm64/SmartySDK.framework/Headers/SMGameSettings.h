#import <Foundation/Foundation.h>


@interface SMGameSettings : NSObject

@property (atomic, assign, readonly) NSInteger numOfPlayers;

- (instancetype)initWithNumOfPlayers:(NSInteger)numOfPlayers;

@end
