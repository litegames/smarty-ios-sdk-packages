#import <Foundation/Foundation.h>

@class SMData;
@class SMDataWrapper;


@interface SMDataArray : NSObject

@property (nonatomic, assign, readonly) NSInteger size;

- (void)removeElementAtIndex:(NSInteger)index;

- (NSInteger)elementTypeAtIndex:(NSInteger)index;

- (NSNumber *)boolAtIndex:(NSInteger)index;
- (void)addBool:(NSNumber *)value;

- (NSNumber *)byteAtIndex:(NSInteger)index;
- (NSNumber *)unsignedByteAtIndex:(NSInteger)index;
- (void)addByte:(NSNumber *)value;

- (NSNumber *)shortAtIndex:(NSInteger)index;
- (void)addShort:(NSNumber *)value;

- (NSNumber *)intAtIndex:(NSInteger)index;
- (void)addInt:(NSNumber *)value;

- (NSNumber *)longAtIndex:(NSInteger)index;
- (void)addLong:(NSNumber *)value;

- (NSNumber *)floatAtIndex:(NSInteger)index;
- (void)addFloat:(NSNumber *)value;

- (NSNumber *)doubleAtIndex:(NSInteger)index;
- (void)addDouble:(NSNumber *)value;

- (NSString *)stringAtIndex:(NSInteger)index;
- (void)addString:(NSString *)value;

- (NSArray<NSNumber *> *)boolArrayAtIndex:(NSInteger)index;
- (void)addBoolArray:(NSArray<NSNumber *> *)value;

- (NSData *)byteArrayAtIndex:(NSInteger)index;
- (void)addByteArray:(NSData *)value;

- (NSArray<NSNumber *> *)shortArrayAtIndex:(NSInteger)index;
- (void)addShortArray:(NSArray<NSNumber *> *)value;

- (NSArray<NSNumber *> *)intArrayAtIndex:(NSInteger)index;
- (void)addIntArray:(NSArray<NSNumber *> *)value;

- (NSArray<NSNumber *> *)longArrayAtIndex:(NSInteger)index;
- (void)addLongArray:(NSArray<NSNumber *> *)value;

- (NSArray<NSNumber *> *)floatArrayAtIndex:(NSInteger)index;
- (void)addFloatArray:(NSArray<NSNumber *> *)value;

- (NSArray<NSNumber *> *)doubleArrayAtIndex:(NSInteger)index;
- (void)addDoubleArray:(NSArray<NSNumber *> *)value;

- (NSArray<NSString *> *)stringArrayAtIndex:(NSInteger)index;
- (void)addStringArray:(NSArray<NSString *> *)value;

- (SMDataArray *)dataArrayAtIndex:(NSInteger)index;
- (void)addDataArray:(SMDataArray *)value;

- (SMData *)dataAtIndex:(NSInteger)index;
- (void)addData:(SMData *)value;

- (SMDataWrapper *)dataWrapperAtIndex:(NSInteger)index;
- (void)addDataWrapper:(SMDataWrapper *)value;

@end
