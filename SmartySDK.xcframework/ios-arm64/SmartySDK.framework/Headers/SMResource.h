#import <UIKit/UIKit.h>

#import <SmartySDK/SMResourceSize.h>


@interface SMResource : NSObject

@property (nonatomic, assign, readonly) SMResourceSize size;
@property (nonatomic, strong, readonly) NSString *url;
@property (nonatomic, strong, readonly) NSString *imageName;
@property (nonatomic, strong, readonly) NSBundle *imageBundle;
@property (nonatomic, assign, readonly) BOOL local;

- (void)loadWithCompletionHandler:(void (^)(UIImage *image))completionHandler;

@end
