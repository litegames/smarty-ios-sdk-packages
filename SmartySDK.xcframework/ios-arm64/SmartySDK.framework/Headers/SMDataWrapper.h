#import <Foundation/Foundation.h>

@interface SMDataWrapper : NSObject

@property (nonatomic, assign, readonly) NSInteger type;
@property (nonatomic, strong, readonly) id data;

@end
