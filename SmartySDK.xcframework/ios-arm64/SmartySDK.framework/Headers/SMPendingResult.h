#import <Foundation/Foundation.h>

@class SMError;


@protocol SMPendingResult <NSObject>

@property (nonatomic, assign, readonly) BOOL done;
@property (nonatomic, assign, readonly) BOOL cancelled;

@property (nonatomic, strong, readonly) id result;
@property (nonatomic, strong, readonly) SMError *error;

@property (nonatomic, copy) void (^onFinishCallback)(id result, SMError *error);
@property (nonatomic, copy) void (^onCancelCallback)(void);

- (void)cancel;

@end
