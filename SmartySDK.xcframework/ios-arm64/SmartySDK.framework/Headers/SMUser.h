#import <Foundation/Foundation.h>

@class SMUserProfile;
@class SMUserVariable;


@interface SMUser : NSObject

@property (nonatomic, assign, readonly) NSInteger id;
@property (nonatomic, strong, readonly) NSString *name;
@property (nonatomic, strong, readonly) SMUserProfile *profile;
@property (nonatomic, assign, readonly) BOOL isItMe;

@property (nonatomic, strong, readonly) NSArray<SMUserVariable *> *userVariables;

- (SMUserVariable *)userVariableWithName:(NSString *)name;
- (void)setUserVariables:(NSArray<SMUserVariable *> *)userVariables;

@end
