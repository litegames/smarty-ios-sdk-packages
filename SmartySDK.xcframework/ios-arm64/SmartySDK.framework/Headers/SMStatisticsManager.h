#import <Foundation/Foundation.h>

@protocol SMPendingResult;


@interface SMStatisticsManager : NSObject

- (id<SMPendingResult>)myStatistics;

@end
