#import <Foundation/Foundation.h>


@interface SMUserStatistics : NSObject

@property (nonatomic, assign, readonly) NSInteger wins;
@property (nonatomic, assign, readonly) NSInteger losses;
@property (nonatomic, assign, readonly) NSInteger ties;

@end
