#import <Foundation/Foundation.h>

@protocol SMGGameState;


@protocol SMGGame <NSObject>

- (id<SMGGameState>)processPlayersGameStates:(NSArray<id<SMGGameState>> *)playersGameStates;

- (void)playerQuit:(NSInteger)playerId;

- (BOOL)finished;

- (id<SMGGameState>)gameState;

@end
