#import <Foundation/Foundation.h>

#import <SmartySDK/SMErrorCodes.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^SMErrorReporter)(NSError *error);


@interface SMErrorHandler : NSObject

@property (nonatomic, copy, nonnull) SMErrorReporter errorReporter;

+ (nonnull SMErrorReporter)defaultErrorReporter;

- (void)reportErrorWithCode:(NSInteger)code msg:(NSString *)msg;

@end

NS_ASSUME_NONNULL_END
