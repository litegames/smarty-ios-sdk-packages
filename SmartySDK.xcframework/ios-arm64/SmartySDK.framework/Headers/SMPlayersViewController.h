#import <UIKit/UIKit.h>

@class SMPlayersViewController;


@protocol SMPlayersViewControllerDelegate <NSObject>

- (void)playersViewControllerDidDismiss:(SMPlayersViewController *)playersViewController;

@optional
- (void)playersViewController:(SMPlayersViewController *)playersViewController didPickUserWithId:(NSInteger)userId;

@end


@interface SMPlayersViewController : UIViewController

@property (nonatomic, weak) id<SMPlayersViewControllerDelegate> delegate;

- (instancetype)init;
- (instancetype)initForPickUser;

@end
