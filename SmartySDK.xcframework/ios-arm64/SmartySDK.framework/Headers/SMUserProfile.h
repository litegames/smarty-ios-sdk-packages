#import <Foundation/Foundation.h>

@class SMUser;
@class SMUserProfile;


extern NSInteger const kSMUserProfileVariableGenderMale;
extern NSInteger const kSMUserProfileVariableGenderFemale;
extern NSInteger const kSMUserProfileVariableGenderOther;


@protocol SMUserProfileUpdateListener <NSObject>

- (void)userProfileDidUpdate:(SMUserProfile *)profile;

@end


@interface SMUserProfile : NSObject

@property (nonatomic, strong, readonly) NSString *displayName;
@property (nonatomic, strong, readonly) NSString *avatar;
@property (nonatomic, strong, readonly) NSNumber *gender;
@property (nonatomic, strong, readonly) NSNumber *yearOfBirth;
@property (nonatomic, strong, readonly) NSString *city;
@property (nonatomic, strong, readonly) NSString *country;
@property (nonatomic, strong, readonly) NSString *email;

- (BOOL)containsUpdateListener:(id<SMUserProfileUpdateListener>)listener;
- (void)addUpdateListener:(id<SMUserProfileUpdateListener>)listener;
- (void)removeUpdateListener:(id<SMUserProfileUpdateListener>)listener;

@end
