#import <Foundation/Foundation.h>

#import <SmartySDK/SMPendingResult.h>


@interface SMMatchHistoryManager : NSObject

- (id<SMPendingResult>)reportMatchWithWinner:(BOOL)winner score:(long long)score;

@end
