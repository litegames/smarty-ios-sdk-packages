#import <Foundation/Foundation.h>

extern NSErrorDomain const SMSmartyErrorDomain;

// Value that was once commited shouldn't be removed, for future documentation.
//   Mark it as deprecated though if necessary.
// Reserve, if possible, a 1000 value range for each category
// (for example 1000-1999 for one category, 2000-2999 for next one and so on...).
typedef NS_ERROR_ENUM(SMSmartyErrorDomain, SMSmartyErrorCode) {

    // -- SMRoom --
    SMSmartyErrorRoomUserNotInRoomReceivedProfile = 1000,
    SMSmartyErrorRoomUserNotInRoomExitsRoom = 1001,
    SMSmartyErrorRoomUserWaitingForProfileEntersRoom = 1002,
    SMSmartyErrorRoomUserInRoomEntersRoom = 1003,
    SMSmartyErrorRoomUserInRoomReceivedProfile = 1004,
};
