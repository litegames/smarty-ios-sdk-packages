#import <UIKit/UIKit.h>


@class SMMatchHistoryViewController;

@protocol SMMatchHistoryViewControllerDelegate <NSObject>

- (void)matchHistoryViewControllerDidDismiss:(SMMatchHistoryViewController *)matchHistoryViewController;

@end


@interface SMMatchHistoryViewController : UIViewController

@property (nonatomic, weak) id<SMMatchHistoryViewControllerDelegate> delegate;

@end
