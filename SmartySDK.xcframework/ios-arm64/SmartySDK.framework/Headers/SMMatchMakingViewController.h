#import <UIKit/UIKit.h>

@class SMGameRoom;
@class SMGameSettings;
@class SMMatchMakingViewController;


@protocol SMMatchMakingViewControllerDelegate <NSObject>

- (void)matchMakingViewControllerDidDismiss:(SMMatchMakingViewController *)matchMakingViewController;

@optional
- (void)matchMakingViewController:(SMMatchMakingViewController *)matchMakingViewController didJoinGameRoomWithId:(NSInteger)gameRoomId;

@end


@interface SMMatchMakingViewController : UIViewController

@property (strong, nonatomic) id<SMMatchMakingViewControllerDelegate> delegate;

- (instancetype)initForCreateAndJoinLobbyWithGameSettings:(SMGameSettings *)gameSettings inviteesIds:(NSArray<NSNumber *> *)inviteesIds;
- (instancetype)initForJoinLobbyWithLobbyRoomId:(NSInteger)lobbyRoomId lobbyRoomPassword:(NSString *)lobbyRoomPassword;

@end
