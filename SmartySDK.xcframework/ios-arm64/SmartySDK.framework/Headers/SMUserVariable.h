#import <Foundation/Foundation.h>

@interface SMUserVariable : NSObject

+ (instancetype)userVariableWithName:(NSString *)name boolValue:(NSNumber *)boolValue;
+ (instancetype)userVariableWithName:(NSString *)name intValue:(NSNumber *)intValue;

@property (nonatomic, strong, readonly) NSString *name;

@property (nonatomic, strong, readonly) NSNumber *boolValue;
@property (nonatomic, strong, readonly) NSNumber *intValue;

@end
