#import <Foundation/Foundation.h>


extern NSString *const kSMErrorCodeServerError;
extern NSString *const kSMErrorCodeInvalidRequestData;

extern NSString *const kSMErrorCodeValidationErrorEmptyValueNotAllowed;
extern NSString *const kSMErrorCodeValidationErrorValueWrongLength;
extern NSString *const kSMErrorCodeValidationErrorInvalidEmailFormat;
extern NSString *const kSMErrorCodeValidationErrorValueOutOfSet;
extern NSString *const kSMErrorCodeValidationErrorValueTooShort;

extern NSString *const kSMErrorCodeUserOldPasswordIsInvalid;

extern NSString *const kSMErrorCodeUserNameAlreadyTaken;
extern NSString *const kSMErrorCodeUserNameWrongFormat;

extern NSString *const kSMErrorCodeUserProfileVariableUnknown;
extern NSString *const kSMErrorCodeUserProfileVariableWrongValueType;
extern NSString *const kSMErrorCodeUserEmailAlreadyInUse;
extern NSString *const kSMErrorCodeFacebookAccountAlreadyConnected;

extern NSString *const kSMErrorCodeLeaderboardUnknown;

extern NSString *const kSMErrorCodeInternalError;


@interface SMError : NSObject

@property (nonatomic, assign, readonly) NSString *id;

@end
