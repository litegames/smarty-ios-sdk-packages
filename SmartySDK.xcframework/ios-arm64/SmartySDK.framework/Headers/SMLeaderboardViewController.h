#import <UIKit/UIKit.h>


@class SMLeaderboardViewController;

@protocol SMLeaderboardViewControllerDelegate <NSObject>

- (void)leaderboardViewControllerDidDismiss:(SMLeaderboardViewController *)leaderboardViewController;

@end


@interface SMLeaderboardViewController : UIViewController

@property (nonatomic, weak) id<SMLeaderboardViewControllerDelegate> delegate;

- (instancetype)initWithLeaderboardIdentifier:(NSString *)leaderboardIdentifier;

@end
