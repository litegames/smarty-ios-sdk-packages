#import <Foundation/Foundation.h>

@class SMDataArray;
@class SMDataWrapper;


typedef NS_ENUM(NSUInteger, SMDataElementType) {
    kSMDataElementTypeNull = 0,
    kSMDataElementTypeBool = 1,
    kSMDataElementTypeByte = 2,
    kSMDataElementTypeShort = 3,
    kSMDataElementTypeInt = 4,
    kSMDataElementTypeLong = 5,
    kSMDataElementTypeFloat = 6,
    kSMDataElementTypeDouble = 7,
    kSMDataElementTypeString = 8,
    kSMDataElementTypeBoolArray = 9,
    kSMDataElementTypeByteArray = 10,
    kSMDataElementTypeShortArray = 11,
    kSMDataElementTypeIntArray = 12,
    kSMDataElementTypeLongArray = 13,
    kSMDataElementTypeFloatArray = 14,
    kSMDataElementTypeDoubleArray = 15,
    kSMDataElementTypeStringArray = 16,
    kSMDataElementTypeDataArray = 17,
    kSMDataElementTypeData = 18,
};


@interface SMData : NSObject <NSCopying>

@property (nonatomic, assign, readonly) NSInteger size;
@property (nonatomic, strong, readonly) NSSet<NSString *> *keys;

- (instancetype)init;

- (BOOL)containsElementWithKey:(NSString *)key;
- (void)removeElementWithKey:(NSString *)key;
- (NSInteger)elementTypeWithKey:(NSString *)key;

- (NSNumber *)boolForKey:(NSString *)key;
- (void)setBool:(NSNumber *)value forKey:(NSString *)key;

- (NSNumber *)byteForKey:(NSString *)key;
- (void)setByte:(NSNumber *)value forKey:(NSString *)key;

- (NSNumber *)shortForKey:(NSString *)key;
- (void)setShort:(NSNumber *)value forKey:(NSString *)key;

- (NSNumber *)intForKey:(NSString *)key;
- (void)setInt:(NSNumber *)value forKey:(NSString *)key;

- (NSNumber *)longForKey:(NSString *)key;
- (void)setLong:(NSNumber *)value forKey:(NSString *)key;

- (NSNumber *)floatForKey:(NSString *)key;
- (void)setFloat:(NSNumber *)value forKey:(NSString *)key;

- (NSNumber *)doubleForKey:(NSString *)key;
- (void)setDouble:(NSNumber *)value forKey:(NSString *)key;

- (NSString *)stringForKey:(NSString *)key;
- (void)setString:(NSString *)value forKey:(NSString *)key;

- (NSArray<NSNumber *> *)boolArrayForKey:(NSString *)key;
- (void)setBoolArray:(NSArray<NSNumber *> *)value forKey:(NSString *)key;

- (NSData *)byteArrayForKey:(NSString *)key;
- (void)setByteArray:(NSData *)value forKey:(NSString *)key;

- (NSArray<NSNumber *> *)shortArrayForKey:(NSString *)key;
- (void)setShortArray:(NSArray<NSNumber *> *)value forKey:(NSString *)key;

- (NSArray<NSNumber *> *)intArrayForKey:(NSString *)key;
- (void)setIntArray:(NSArray<NSNumber *> *)value forKey:(NSString *)key;

- (NSArray<NSNumber *> *)longArrayForKey:(NSString *)key;
- (void)setLongArray:(NSArray<NSNumber *> *)value forKey:(NSString *)key;

- (NSArray<NSNumber *> *)floatArrayForKey:(NSString *)key;
- (void)setFloatArray:(NSArray<NSNumber *> *)value forKey:(NSString *)key;

- (NSArray<NSNumber *> *)doubleArrayForKey:(NSString *)key;
- (void)setDoubleArray:(NSArray<NSNumber *> *)value forKey:(NSString *)key;

- (NSArray<NSString *> *)stringArrayForKey:(NSString *)key;
- (void)setStringArray:(NSArray<NSString *> *)value forKey:(NSString *)key;

- (SMDataArray *)dataArrayForKey:(NSString *)key;
- (void)setDataArray:(SMDataArray *)value forKey:(NSString *)key;

- (SMData *)dataForKey:(NSString *)key;
- (void)setData:(SMData *)value forKey:(NSString *)key;

- (SMDataWrapper *)dataWrapperForKey:(NSString *)key;
- (void)setDataWrapper:(SMDataWrapper *)value forKey:(NSString *)key;

@end
