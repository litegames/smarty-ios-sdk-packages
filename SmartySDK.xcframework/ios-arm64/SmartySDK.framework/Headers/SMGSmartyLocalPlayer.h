#import <Foundation/Foundation.h>

#import <SmartySDK/SmartySDK.h>

#import <SmartySDK/SMGGamePlayer.h>


@interface SMGSmartyLocalPlayer : NSObject <SMGGamePlayer>

- (instancetype)initWithSmarty:(SMSmarty *)smarty gameRoom:(SMGameRoom *)gameRoom gamePlayer:(id<SMGGamePlayer>)gamePlayer;

@end
