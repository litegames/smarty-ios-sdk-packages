#import <Foundation/Foundation.h>

#import <SmartySDK/SMPendingResult.h>

@interface SMLeaderboardManager : NSObject

- (id<SMPendingResult>)submitScoreToLeaderboardWithIdentifier:(NSString *)leaderboardIdentifier score:(long long)score;

@end
