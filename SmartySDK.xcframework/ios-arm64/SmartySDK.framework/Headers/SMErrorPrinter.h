#import <Foundation/Foundation.h>

@class SMError;


@interface SMErrorPrinter : NSObject

- (NSString *)print:(SMError *)error;

@end
