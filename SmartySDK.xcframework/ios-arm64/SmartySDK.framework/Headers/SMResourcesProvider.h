#import <UIKit/UIKit.h>

#import <SmartySDK/SMResourceSize.h>

@class SMUserProfile;
@class SMResource;


@interface SMResourcesProvider : NSObject

- (SMResource *)avatarResourceForUserProfile:(SMUserProfile *)profile withSize:(SMResourceSize)size;
- (SMResource *)avatarForOfflineUsageResourceWithSize:(SMResourceSize)size;
- (SMResource *)avatarForAIPlayerResourceWithSize:(SMResourceSize)size;

- (UIColor *)avatarDominantColorForUserProfile:(SMUserProfile *)profile;
- (UIColor *)avatarForOfflineUsageDominantColor;
- (UIColor *)avatarForAIPlayerDominantColor;

- (NSArray<UIColor *> *)nUniqueAvatarDominantColors:(NSInteger)n;

- (UIImage *)avatarMaskImageWithSize:(SMResourceSize)size;
- (UIImage *)avatarAlphaMaskImageWithSize:(SMResourceSize)size;

- (NSString *)genderNameForGender:(NSNumber *)gender;

- (NSString *)countryNameForCountry:(NSString *)countryCode;

@end
