#import <Foundation/Foundation.h>


@protocol SMGameCreateListener <NSObject>

- (void)onGameCreate:(id)sender inviteesIds:(NSArray<NSNumber *> *)inviteesIds;

@end
