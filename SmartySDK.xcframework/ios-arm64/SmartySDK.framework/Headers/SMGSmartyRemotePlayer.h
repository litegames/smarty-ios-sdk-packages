#import <Foundation/Foundation.h>
#import <SmartySDK/SmartySDK.h>

#import <SmartySDK/SMGGamePlayer.h>

@protocol SMGGameStateSerializer;


@interface SMGSmartyRemotePlayer : NSObject <SMGGamePlayer>

@property (nonatomic, assign) BOOL sendGameStateDiff;

- (instancetype)initWithSmarty:(SMSmarty *)smarty
                      gameRoom:(SMGameRoom *)gameRoom
                      playerId:(NSInteger)playerId
           gameStateSerializer:(id<SMGGameStateSerializer>)gameStateSerializer;

@end
